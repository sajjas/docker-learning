docker wordpress deployments
----------------------------

Link option
-----------

[vagrant@localhost ~]$ sudo su -
Last login: Fri Apr  3 17:05:32 UTC 2020 on pts/0
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
604f45aad65d        mysql:5.7           "docker-entrypoint..."   2 minutes ago       Up 2 minutes        3306/tcp, 33060/tcp    test-mysql
314d1508cf9a        wordpress:latest    "docker-entrypoint..."   5 minutes ago       Up 5 minutes        0.0.0.0:8080->80/tcp   test-wordpress
[root@localhost ~]# docker stop test-wordpress
test-wordpress
[root@localhost ~]# docker rm test-wordpress
test-wordpress
[root@localhost ~]#
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                 NAMES
604f45aad65d        mysql:5.7           "docker-entrypoint..."   2 minutes ago       Up 2 minutes        3306/tcp, 33060/tcp   test-mysql
[root@localhost ~]# docker run -d --name=test-wordpress --link=test-mysql:mysql -p 8080:80 wordpress:latest
4f5a68947bd633149606f15749e9ee496583c26e37cb6b0e96ac7f4b98cea93f
[root@localhost ~]# docker ps
CONTAINER ID        IMAGE               COMMAND                  CREATED             STATUS              PORTS                  NAMES
4f5a68947bd6        wordpress:latest    "docker-entrypoint..."   2 seconds ago       Up 2 seconds        0.0.0.0:8080->80/tcp   test-wordpress
604f45aad65d        mysql:5.7           "docker-entrypoint..."   3 minutes ago       Up 3 minutes        3306/tcp, 33060/tcp    test-mysql
[root@localhost ~]# curl -I http://localhost:8080 -kv
* About to connect() to localhost port 8080 (#0)
*   Trying ::1...
* Connected to localhost (::1) port 8080 (#0)
> HEAD / HTTP/1.1
> User-Agent: curl/7.29.0
> Host: localhost:8080
> Accept: */*
>
< HTTP/1.1 302 Found
HTTP/1.1 302 Found
< Date: Fri, 03 Apr 2020 17:17:40 GMT
Date: Fri, 03 Apr 2020 17:17:40 GMT
< Server: Apache/2.4.38 (Debian)
Server: Apache/2.4.38 (Debian)
< X-Powered-By: PHP/7.3.16
X-Powered-By: PHP/7.3.16
< Expires: Wed, 11 Jan 1984 05:00:00 GMT
Expires: Wed, 11 Jan 1984 05:00:00 GMT
< Cache-Control: no-cache, must-revalidate, max-age=0
Cache-Control: no-cache, must-revalidate, max-age=0
< X-Redirect-By: WordPress
X-Redirect-By: WordPress
< Location: http://localhost:8080/wp-admin/install.php
Location: http://localhost:8080/wp-admin/install.php
< Content-Type: text/html; charset=UTF-8
Content-Type: text/html; charset=UTF-8

<
* Connection #0 to host localhost left intact
[root@localhost ~]# docker exec -it test-wordpress bash
root@4f5a68947bd6:/var/www/html# cat /etc/hosts
127.0.0.1       localhost
::1     localhost ip6-localhost ip6-loopback
fe00::0 ip6-localnet
ff00::0 ip6-mcastprefix
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
172.17.0.3      mysql 604f45aad65d test-mysql
172.17.0.2      4f5a68947bd6
root@4f5a68947bd6:/var/www/html# 


The use of Link is, even if we restart/recreate the mysql docker container and if it gets 
the new ip address, link will take care of updating the connectivity in wordpress container
as well. so that connection will not be broken between mysql and wordpress containers.
